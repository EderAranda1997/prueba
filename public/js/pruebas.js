// const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;

// const sendForm = () => {
//     fetch('pruebas', {
//     method: 'POST',
//     body: JSON.stringify({
//         consulta:{
//             0:{
//                 tabla: 'user',
//                 ids: [1,2,3],
//                 cant: [30,40],
//                 columnas: ['nombres', 'apellidos'],
//                 consulta: {
//                     0:{
//                         tabla: 'contactos',
//                         cant: 4,
//                         columnas: ['user_id','telefone', 'direction']

//                         // consulta: {
//                         //     0:{
//                         //         tabla: 'contactos',
//                         //         ids: [3, 4],
//                         //         columnas: ['telefone', 'direction'],                                
//                         //     },
                        
//                     },
//                     // 1:{
//                     //     tabla: 'contactos',
//                     //     ids: [3, 4],
//                     //     columnas: ['telefone', 'direction'],
//                     // }
//                 }
//             },
//             // 1:{
//             //     tabla: 'contactos',
//             //     ids: [1,2],
//             //     cant: 2,
//             //     columnas: ['telefone', 'direction'],
//             // },
//             // 1:{
//             //     tabla: 'productos',
//             //     ids: [1,2,3],
//             //     cant: 2,
//             //     columnas: ['precio', 'cantidad'],
//             // },
//         }
//     }),
//     headers: {
//         'Content-Type': 'application/json',
//         'Accept': 'application/json',
//         "X-CSRF-Token": csrfToken}
//     })
//     .then(function(res) {
//         return res.json();
//     })
//     .then(function(res) {
//         console.log(res);
//     })
//     .catch(function(err) {
//         console.log(err);
//     })
// };

// sendForm();


// query: {
//     0:{
//         tabla: 'contactos',
//         ids: [1],
//         columnas: ['celular', 'fijo'],
//         query: {

//         }
//     },
//     1:{
//         tabla: 'finanzas',
//         ids: [2],
//         columnas: ['n_tarjeta']
//     }
// }







const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;

const sendForm = (consulta, ejecutar) => {
    fetch('pruebas', {
    method: 'POST',
    body: JSON.stringify(consulta),
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        "X-CSRF-Token": csrfToken}
    })
    .then(function(res) {
        return res.json();
    })
    .then(function(res) {
        ejecutar(res, consulta);
    })
    .catch(function(err) {
        console.log(err);
    })
};

// El modelo de la consulta

let consultaUser = {
    consulta:{
        0:{
            cod: generarCodigo(),
            tabla: 'users',
            ids: [1,2,3],
            columnas: ['nombres', 'apellidos'],
            consulta: {
                0:{
                    cod: generarCodigo(),
                    tabla: 'contacts',
                    ids: [1,2],
                    columnas: ['telefone', 'direction']                   
                },
            }
        },
    }
}

// La funcion que trabaja los datos de la respuesta
function usuarios (res, consulta){
    console.log(res);
}


// Declaramos las consultaUser

// $("#consultar").addEventListener('click', function(){
    // sendForm(consultaUser, usuarios);
// })



function generarCodigo() {
    function random() {
        return Math.random().toString(36).substr(2); // Eliminar `0.`
    };

    return random()+random();
}



let varia = {

    "id": '2',
    "nombres": 'joel',
    "apellidos": 'angulo',
    "contacto": {
        "id": '1',
        "numero": '987892456',
        "dispositivo": {
            "id": '2',
            "modelo": 'as1256',
            "marca": 'sony',
        }
    }
}


let v = {

    user: {
        "2":{
            "id": '2',
            "dependencia": "",
            "nombres": 'joel',
            "apellidos": 'angulo',
        }
    },
    contacto: {
        "1":{
            "id": '1',
            "user_id": '2',
            "dependencia": "user_",
            "numero": '987892456',
            "codDependencia": 'user',
        },
        "2":{
            "id": '2',
            "user_id": '2',
            "dependencia": "user_",
            "numero": '987892456',
            "codDependencia": 'user',
        }
    },
    dispositivo: {
        "2": {        
            "id": '2',
            "contacto_id": "1",
            "dependencia": "contacto_",
            "modelo": 'as1256',
            "marca": 'sony',
            "codDependencia": 'contacto',
        },
        "3": {        
            "id": '3',
            "contacto_id": "2",
            "dependencia": "contacto_",
            "modelo": 'as1256',
            "marca": 'sony',
            "codDependencia": 'contacto',
        }
    }
}

let keysReverse = Object.keys(v).reverse();

function recorrer(v) {
    
    let objCreado = {};

    keysReverse.forEach(function(key){
        for (const k in v[key]) {
            if (v[key].hasOwnProperty(k)) {
                const element = v[key][k];
                let dependencia = element.dependencia;
                let id_dependencia = element[dependencia+'id'];
                objCreado[element.codDependencia] = '';
                objCreado[element.codDependencia][id_dependencia][key] = v[key];
            }
        }
    });

    return objCreado;
}

console.log(recorrer(v));