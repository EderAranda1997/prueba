// // const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;

// // const sendForm = () => {
// //     fetch('pruebas', {
// //     method: 'POST',
// //     body: JSON.stringify({
// //         consulta:{
// //             0:{
// //                 tabla: 'user',
// //                 ids: [1,2],
// //                 cant: 2,
// //                 columnas: ['nombres', 'apellidos'],
// //                 consulta: {
// //                     0:{
// //                         tabla: 'contactos',
// //                         ids: [1, 2],
// //                         columnas: ['telefone', 'direction']
// //                         // consulta: {
// //                         //     0:{
// //                         //         tabla: 'contactos',
// //                         //         ids: [3, 4],
// //                         //         columnas: ['telefone', 'direction'],                                
// //                         //     },
                        
// //                     },
// //                     // 1:{
// //                     //     tabla: 'contactos',
// //                     //     ids: [3, 4],
// //                     //     columnas: ['telefone', 'direction'],
// //                     // }
// //                 }
// //             },
// //             // 1:{
// //             //     tabla: 'contactos',
// //             //     ids: [1,2],
// //             //     cant: 2,
// //             //     columnas: ['telefone', 'direction'],
// //             // },
// //             // 1:{
// //             //     tabla: 'productos',
// //             //     ids: [1,2,3],
// //             //     cant: 2,
// //             //     columnas: ['precio', 'cantidad'],
// //             // },
// //         }
// //     }),
// //     headers: {
// //         'Content-Type': 'application/json',
// //         'Accept': 'application/json',
// //         "X-CSRF-Token": csrfToken}
// //     })
// //     .then(function(res) {
// //         return res.json();
// //     })
// //     .then(function(res) {
// //         console.log(res);
// //     })
// //     .catch(function(err) {
// //         console.log(err);
// //     })
// // };

// // sendForm();


// // query: {
// //     0:{
// //         tabla: 'contactos',
// //         ids: [1],
// //         columnas: ['celular', 'fijo'],
// //         query: {

// //         }
// //     },
// //     1:{
// //         tabla: 'finanzas',
// //         ids: [2],
// //         columnas: ['n_tarjeta']
// //     }
// // }



// //Crear un array
// let frutas = ["Manzana", "Banana"]

// //Recorrer un array
// frutas.forEach(function(elemento, indice) {
//     // console.log(elemento, indice);
// })

// //Añadir un elemento al final de un Array
// let nuevaLongitud = frutas.push('Naranja') // Añade "Naranja" al final
// // console.log(frutas);

// //Añadir un elemento al final de un Array
// let ultimo = frutas.pop() // Elimina "Naranja" del final

// //Añadir un elemento al principio de un Array
// let nuevaLongitud2 = frutas.unshift('Fresa') // Añade "Fresa" al inicio

// //Eliminar un elemento al principio de un Array
// let primero = frutas.shift() // Elimina "Fresa" del inicio

// //Encontrar el índice de un elemento del Array
// frutas.push('Fresa')
// let pos = frutas.indexOf('Manzana') // (pos) es la posición para abreviar



// //Eliminar un único elemento mediante su posición
// let elementoEliminado = frutas.splice(pos, 2)


// //Eliminar varios elementos a partir de una posición
// let vegetales = ['Repollo', 'Nabo', 'Rábano', 'Zanahoria']

// let posicion = 1, numElementos = 2
// let elementosEliminados = vegetales.splice(posicion, numElementos)

// //Copiar un Array
// let copiaArray = vegetales.slice();

// //LLamando Array
// let decadas = [1950, 1960, 1970, 1980, 1990, 2000, 2010]
//     // console.log(decadas['2'] != decadas['02'])
//     // console.log(decadas['02'])


// //Relación entre length y las propiedades numéricas

// const pre = []
// pre.push('banana', 'manzana', 'pera')

//     // console.log(pre) 
//     // console.log(pre.length) 

// const f = []
//     // f.push('banana', 'manzana', 'pera')
//     // f[5] = 'fresa'
//     // console.log(f[5])           // 'fresa'
//     // console.log(Object.keys(f)) // ['0', '1', '2','5']
//     // console.log(f.length)       // 6
//     // f.length = 2
//     // console.log(Object.keys(f)) // ['0', '1']
//     // console.log(f.length)       // 2


// //Creación de un array a partir de una expresión regular
// const miRe = /d(b+)(d)/i
// const miArray = miRe.exec('cdbBdbsbz')



// const a = Array();
// b = a.push('a', 'g')
// // console.log(a);



// //Propiedades del array
//     //Concatenar arrays
//         const array1 = ['a', 'b', 'c'];
//         const array2 = ['d', 'e', 'f'];
//         const array3 = array1.concat(array2);

//     // console.log(array3);

//     //Copiar elements dentro del mismo array
//         const array4 = ['a', 'b', 'c', 'd', 'e'];
//         // console.log(array4.copyWithin(0, 3, 4));
    

//     //Recorrer clave valor uno a uno en cada impresión
//         const array5 = ['a', 'b', 'c'];
//         const iterator1 = array5.entries();
    
//         // console.log(iterator1.next().value);
//         // // expected output: Array [0, "a"]
//         // console.log(iterator1.next().value);
//         // // expected output: Array [1, "b"]
//         // console.log(iterator1.next().value);
//         // // expected output: Array [1, "c"]
      
        
//     //Determina si todos los elementos en el array satisfacen una condición: array.every(funcion)
//         const isBelowThreshold = (currentValue) => currentValue < 40;
//         const array6 = [1, 30, 39, 29, 10, 13];
//         // console.log(array6.every(isBelowThreshold));
//         // expected output: true


//     //El método fill() cambia todos los elementos en un arreglo por un valor estático, 
//     //desde el índice start (por defecto 0) hasta el índice end (por defecto array.length). 
//     //Devuelve el arreglo modificado

//         const array7 = [1, 2, 3, 4];

//         // fill with 0 from position 2 until position 4
//         // console.log(array7.fill(0, 2, 4));
//         // expected output: [1, 2, 0, 0]

//         // fill with 5 from position 1
//         // console.log(array7.fill(5, 1));
//         // expected output: [1, 5, 5, 5]

//         // console.log(array7.fill(6));
//         // expected output: [6, 6, 6, 6]

    
//     //El método filter() crea un nuevo array con todos los elementos 
//     //que cumplan la condición implementada por la función dada.
//         const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];
//         const result = words.filter(word => word.length > 6);
//         // console.log(result);
//         // expected output: Array ["exuberant", "destruction", "present"]


//     //El método find() devuelve el valor del primer elemento del array que cumple
//     //la función de prueba proporcionada.

//         const array8 = [5, 12, 8, 130, 44];
//         const found = array8.find(element => element > 10);
//         // console.log(found);
//         // expected output: 12
    
//     //El método findIndex() devuelve el índice del primer elemento de un array que cumpla con 
//     //la función de prueba proporcionada. En caso contrario devuelve -1.

//         const array9 = [5, 12, 8, 130, 44];
//         const isLargeNumber = (element) => element > 13;   
//         // console.log(array9.findIndex(isLargeNumber));
//         // expected output: 3
    
//     //El método forEach() ejecuta la función indicada una vez por cada elemento del array.
//         const array10 = ['a', 'b', 'c'];
//         // array10.forEach(element => console.log(element));


//         const array11 = ['a', 'b', 'c'];
//         const iterator = array11.keys();
//         // for (const key of iterator) {
//         //     console.log(key);
//         // }

//         var numbers = [1, 5, 10, 15];
//         var doubles = numbers.map(function(x) {
//         return x * 2;
//         });
//         // console.log(doubles);


//         let tablero = [ 
//             ['T','C','A','D','R','A','C','T'],
//             ['P','P','P','P','P','P','P','P'],
//             [' ',' ',' ',' ',' ',' ',' ',' '],
//             [' ',' ',' ',' ',' ',' ',' ',' '],
//             [' ',' ',' ',' ',' ',' ',' ',' '],
//             [' ',' ',' ',' ',' ',' ',' ',' '],
//             ['p','p','p','p','p','p','p','p'],
//             ['r','c','a','d','r','a','c','t'] ]
          
//         // console.log(tablero.join('\n') + '\n\n')
        
//         // Adelantar dos posiciones el peón de rey
//         tablero[4][4] = tablero[6][4]
//         tablero[6][4] = ' '
//         // console.log(tablero.join('\n'))


//         valores = []
//         for (let x = 0; x < 10; x++){
//             valores.push([
//             2 ** x,
//             2 * x ** 2
//         ])
//         }
//         // console.table(valores)



// //Practicando
//     //1. Crear array
//         let p = [];

//     //2. Asignarle valores
//         let p1 = p.push('a', 'v', 'c');

//     //3. Leer cada elemento del array
//         p.forEach(function(elemento, indice) {
//             // console.log(elemento, indice);
//         })
    

//         function ejemploForIn() {

//                 var dato = [2, 6, 5, 1, 18, 44];      
//                 var msgForNormal = '';       
//                 var msgForIn = '';
                
//                 //For normal      
//                 for (var i=0; i<dato.length; i++) { msgForNormal = msgForNormal + dato[i] + ' - '; }
                
//                 //For in      
//                 for (i in dato) { msgForIn = msgForIn + dato[i] + ' - '; }       
//                 // alert ('msgForNormal contiene ' + msgForNormal + ' y msgForIn contiene '+ msgForIn);        
//         }
//             // ejemploForIn();

//         var numbers = [1, 5, 10, 15];
//         var doubles = numbers.map(function(x) {
//             // console.log(x);
//         });

        
//     // 4. Eliminar y agregar datos
//         var num = numbers.splice(2, 1, 8);
//         // console.log(numbers);
    
//     // 5. Sumar los valores de un array
//         let o1 = [1,2,3]
//         let o2 = [4,5,6]
//         // let o3 = o1 +','+ o2
//         // console.log(o3);

//         let o3=[]

//         for(i = 0; i < o1.length; i++){
//             o3[i] =o1[i]+o2[i];
//         }
//         // console.log(o3);


//     //6. Convertir un array a un objeto

//         // Para array unico
//         let lag = [1,2,3,4,5,6]
//         let obj = {}

//         function convert(array) {
//             let hola = array.keys();

//             function keys(keys) {
//                 for (const k of keys) {
//                     return k;
//                 }
//             }
           
//             for(i = 0; i < array.length; i++){
//                 obj[keys(hola)] = array[i];

//             }
//            console.log(obj);
         
//         }
//         // convert(lag);




//         //Para array doble
//         let a1 = [[1,2],[3,4],[5,6]]
//         let objeto = {}

//         function convert(array) {
//             for(i = 0; i < array.length; i++){
//                 objeto[array[i][0]] = array[i][1];
//             }

//             console.log(objeto);
//         }
//         // convert(a1);

        
//     //7.Cambiar la posicion de un array
//         const mos = ['Jan', 'March', 'April', 'June'];
//         const ol = mos.splice(1,1);
//         const mos2 = mos.splice(3,0,ol[0])
//         // console.log(mos);
    
//     // 8. Validad si existe un elemento dentro de un array
//         let masct = ['perro', 'cat', 'conejo']
//         function mascotas(masc) {
//             if(masc.indexOf('perro') !== -1){
//                 console.log('Perro existe dentro de array')
//             }
//         }
//         // mascotas(masct);

//     // 9. Crear un array muntidimensional automatizadamente
       
//         let x = []
 
//         function multi(p) {
//             for (let i = 0; i < 10; i++){
//                 p.push([
//                 2 ** i,
//                 2 * i ** 2,
//                 3 * i ** 2
//             ])
//             }
//             console.log(p);
//         }

//         // multi(x);




















