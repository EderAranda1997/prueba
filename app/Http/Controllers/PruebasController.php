<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Contact;
use Illuminate\Support\Collection;
use tidy;

class PruebasController extends Controller
{
    public function vista()
    {

    return view('pruebas');
    }

    public function pruebas(Request $request) 
    {

        $consulta = array();
        $consulta['consulta'] =  $request['consulta'];  
        $GLOBALS["datos"] = array();

        function consultas($objetos, $cod_dependencia = null, $dependencia = '',$idsDependencia = null){

            if (array_key_exists('consulta', $objetos) && count($objetos['consulta']) > 0) {
                               
                foreach($objetos['consulta'] as $objeto)
                {
                    
                    $tabla = $objeto['tabla'];
                    $columnas = $objeto['columnas'];
                    $ids = $objeto['ids'];
                    $cod = $objeto['cod'];
                    $GLOBALS['idsNew'] = null;

                    // Se le agrega el id debido a que el select se restringe unicamente a las columnas mencionadas
                    array_push($columnas, 'id');
                    
                    // Si dependencia es igual a '' entonces agregamos la columna de dependencia
                    if ($dependencia !== '') {
                        array_push($columnas, $dependencia.'id');
                    }

                    // Los ids de la tabla anteriormente recorrida
                    if($idsDependencia !== null){
                        $ids = $idsDependencia;  
                    }
            
                    if ($tabla === 'users') {
                        $datos = User::select($columnas)->whereIn($dependencia.'id', $ids)->get();
                        foreach($datos as $dato){
                            $GLOBALS["datos"][$cod]['datos'][$dato->id] = $dato;
                        }
                        $GLOBALS['idsNew'] = array_keys($GLOBALS["datos"][$cod]['datos']);
                        $GLOBALS["datos"][$cod]['codDependencia'] = $cod_dependencia;
                        $dependencia = 'user_';
                    }
                    
                    if ($tabla === 'contacts') {
                        $datos = Contact::select($columnas)->whereIn($dependencia.'id', $ids)->get();
                        foreach($datos as $dato){
                            $GLOBALS["datos"][$cod]['datos'][$dato->id] = $dato;
                        }
                        $GLOBALS['idsNew'] = array_keys($GLOBALS["datos"][$cod]['datos']);
                        $GLOBALS["datos"][$cod]['codDependencia'] = $cod_dependencia;
                        $dependencia = 'contact_';
                    }

                    $cod_dependencia = $cod;
                    
                    consultas($objeto, $cod_dependencia, $dependencia, $GLOBALS['idsNew']);
                }
            }
        }

        consultas($consulta);

        return response()->json($GLOBALS["datos"]);

    }
    

 



}





// $consulta = array();
// $consulta['consulta'] =  $request['consulta'];  
// $GLOBALS["datos"] = array();

// function consultas($objetos, $cod_dependencia = null, $dependencia = '',$idsDependencia = null){

//     if (array_key_exists('consulta', $objetos) && count($objetos['consulta']) > 0) {
                       
//         foreach($objetos['consulta'] as $objeto)
//         {
            
//             $tabla = $objeto['tabla'];
//             $columnas = $objeto['columnas'];
//             $ids = $objeto['ids'];
//             $cod = $objeto['cod'];
//             $GLOBALS['idsNew'] = null;

//             // Se le agrega el id debido a que el select se restringe unicamente a las columnas mencionadas
//             array_push($columnas, 'id');
            
//             // Si dependencia es igual a '' entonces agregamos la columna de dependencia
//             if ($dependencia !== '') {
//                 array_push($columnas, $dependencia.'id');
//             }

//             // Los ids de la tabla anteriormente recorrida
//             if($idsDependencia !== null){
//                 $ids = $idsDependencia;  
//             }
    
//             if ($tabla === 'users') {
//                 $datos = User::select($columnas)->whereIn($dependencia.'id', $ids)->get();
//                 foreach($datos as $dato){
//                     $GLOBALS["datos"][$cod]['datos'][$dato->id] = $dato;
//                 }
//                 $GLOBALS['idsNew'] = array_keys($GLOBALS["datos"][$cod]['datos']);
//                 $GLOBALS["datos"][$cod]['codDependencia'] = $cod_dependencia;
//                 $dependencia = 'user_';
//             }
            
//             if ($tabla === 'contacts') {
//                 $datos = Contact::select($columnas)->whereIn($dependencia.'id', $ids)->get();
//                 foreach($datos as $dato){
//                     $GLOBALS["datos"][$cod]['datos'][$dato->id] = $dato;
//                 }
//                 $GLOBALS['idsNew'] = array_keys($GLOBALS["datos"][$cod]['datos']);
//                 $GLOBALS["datos"][$cod]['codDependencia'] = $cod_dependencia;
//                 $dependencia = 'contact_';
//             }

//             $cod_dependencia = $cod;
            
//             consultas($objeto, $cod_dependencia, $dependencia, $GLOBALS['idsNew']);
//         }
//     }
// }

// consultas($consulta);

// return response()->json($GLOBALS["datos"]);

// }