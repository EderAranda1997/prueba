<?php

namespace App;
use App\User;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'telefone', 'direction', 
    ];

    public function user()
    {
        return $this->belongTo(User::class);
    }
}
